import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import PersonalCenter from '@/components/zerone/PersonalCenter'
import FriendsList from '@/components/zerone/FriendsList'
import CouponList from '@/components/zerone/CouponList'
import ShopList from '@/components/zerone/ShopList'
import IntegralList from '@/components/zerone/IntegralList'
import IntegralListFuture from '@/components/zerone/IntegralListFuture'
import BinTixianInfo from '@/components/zerone/BinTixianInfo'
import TiXian from '@/components/zerone/TiXian'
import SetPassword from '@/components/zerone/SetPassword'
import ResetPassword from '@/components/zerone/ResetPassword'
import BinZerone from '@/components/zerone/BinZerone'
import Follow from '@/components/zerone/Follow'
import StreamLog from '@/components/zerone/StreamLog'
import FanQrcode from '@/components/zerone/FanQrcode'
import WithdrawList from '@/components/zerone/WithdrawList'
import BinPeople from '@/components/zerone/BinPeople'
import BindFans from '@/components/zerone/BindFans'

import ChoiceType from '@/components/ChoiceType'
import index from '@/components/index'
import SelectAddress from '@/components/SelectAddress'
import ShopDetails from '@/components/ShopDetails'
import Dynamic from '@/components/Dynamic'
import CouponDetails from '@/components/CouponDetails'
import author from '@/components/author'
import SuccessfulPayment from '@/components/SuccessfulPayment'

import BusinessIndex from '@/components/businesscircle/BusinessIndex'
import BusinessComment from '@/components/businesscircle/BusinessComment'
import BusinessDetails from '@/components/businesscircle/BusinessDetails'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/shop',
  routes: [
      //meta自定义判断显示或隐藏底部导航
      //navShow-页面的foot导航是否显示
      //index 页面切换z-index层数
      //选择种类
      {
          path: '/index',
          name: 'ChoiceType',
          component: ChoiceType,
          meta:{
              navShow:false,
              index:2
          }
      },
      //首页
      {
          path: '/',
          name: 'index',
          component: index,
          meta:{
              navShow:true,
              index:1,
              fenxiang:{
                  title:"恭喜你获得了36张优惠券！",
                  desc:"进零壹坊领取与分享优惠券，消费时既有折扣又有优惠红包抵扣，还能分享给好友赚钱。上零壹坊就能领取了！"
              }
          }
      },
      //选择地址
      {
          path: '/selectadress',
          name: 'SelectAddress',
          component: SelectAddress,
          meta:{
              navShow:false,
              index:3
          }
      },
      //店铺详情
      {
          path: '/shopdetails',
          name: 'ShopDetails',
          component: ShopDetails,
          meta:{
              navShow:true,
              index:4
          }
      },
      //店铺动态
      {
          path: '/dynamic',
          name: 'Dynamic',
          component: Dynamic,
          meta:{
              navShow:true,
              index:5
          }
      },
      //优惠券详情
      {
          path: '/coupondetails',
          name: 'CouponDetails',
          component: CouponDetails,
          meta:{
              navShow:false,
              index:6,
              fenxiang:{
                  title:"您有一张新的优惠券请领取",
                  desc:"零壹坊官方回馈用户提供福利，优惠券最高高至100元~"
              }
          }
      },
      //付款成功
      {
          path: '/successpm',
          name: 'SuccessfulPayment',
          component: SuccessfulPayment,
          meta:{
              navShow:false,
              index:24
          }
      },
      //个人中心
        {
          path: '/personalcenter',
          name: 'PersonalCenter',
          component: PersonalCenter,
          meta:{
              navShow:true,
              index:7
          }
      },
      //推荐好友列表
      {
          path:'/friendslist',
          name:'FriendsList',
          component:FriendsList,
          meta:{
              navShow:false,
              index:8
          }
      },
      //优惠券列表
      {
          path:'/couponlist',
          name:'CouponList',
          component:CouponList,
          meta:{
              navShow:false,
              index:9
          }
      },
      //推荐商家列表
      {
          path:'/shoplist',
          name:'ShopList',
          component:ShopList,
          meta:{
              navShow:false,
              index:10
          }
      },
      //提成记录
      {
          path:'/integrallist',
          name:'IntegralList',
          component:IntegralList,
          meta:{
              navShow:false,
              index:22
          }
      },
      //未到账提成
      {
          path:'/integrallistfuture',
          name:'IntegralListFuture',
          component:IntegralListFuture,
          meta:{
              navShow:false,
              index:11
          }
      },
      //绑定提现信息
      {
          path:'/bintixianinfo',
          name:'BinTixianInfo',
          component:BinTixianInfo,
          meta:{
              navShow:false,
              index:12
          }
      },
      //提现页面
      {
          path:'/tixian',
          name:'TiXian',
          component:TiXian,
          meta:{
              navShow:false,
              index:13
          }
      },
      //设置安全密码
      {
          path:'/setpassword',
          name:'SetPassword',
          component:SetPassword,
          meta:{
              navShow:false,
              index:14
          }
      },
      //重置安全密码
      {
          path:'/resetpassword',
          name:'ResetPassword',
          component:ResetPassword,
          meta:{
              navShow:false,
              index:15
          }
      },
      //绑定零壹账号
      {
          path:'/binzerone',
          name:'BinZerone',
          component:BinZerone,
          meta:{
              navShow:false,
              index:16
          }
      },
      //我的关注
      {
          path:'/follow',
          name:'Follow',
          component:Follow,
          meta:{
              navShow:false,
              index:17
          }
      },
      //授权中间页面
      {
          path:'/author',
          name:'author',
          component:author,
          meta:{
              navShow:false,
              index:18
          }
      },
      //消费账单
      {
          path:'/streamlog',
          name:'StreamLog',
          component:StreamLog,
          meta:{
              navShow:false,
              index:19
          }
      },
      //粉丝推广码
      {
          path:'/fanqrcode',
          name:'FanQrcode',
          component:FanQrcode,
          meta:{
              navShow:false,
              index:20
          }
      },
      //提现记录
      {
          path:'/withdrawlist',
          name:'WithdrawList',
          component:WithdrawList,
          meta:{
              navShow:false,
              index:21
          }
      },
      {
          path:'/binpeople',
          name:'BinPeople',
          component:BinPeople,
          meta:{
              navShow:false,
              index:22
          }
      },
      //中间件绑定粉丝
      {
          path:'/bindfans',
          name:'BindFans',
          component:BindFans,
          meta:{
              navShow:false,
              index:23
          }
      },
      //攻略首页
      {
          path:'/businessindex',
          name:'BusinessIndex',
          component:BusinessIndex,
          meta:{
              navShow:true,
              index:25
          }
      },
      //攻略详情
      {
          path:'/businessdetails',
          name:'BusinessDetails',
          component:BusinessDetails,
          meta:{
              navShow:false,
              index:26
          }
      },
      //攻略评论页
      {
          path:'/businesscomment',
          name:'BusinessComment',
          component:BusinessComment,
          meta:{
              navShow:false,
              index:27
          }
      },

  ]
})
