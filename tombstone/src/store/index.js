import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)  //注册Vuex

// 定义常量    如果访问他的话，就叫访问状态对象
const state = {
    userInfo:{
        nickname:"",
        headimgurl:""
    },
    //记录要跳转的链接
    toUrl:"",
    // //测试服务器地址*
    host:"https://zerone.01nnt.com",
    //测试前端地址*
    static_host:"https://static.01nnt.com",
    // //正式服务器地址*
    // host:"https://f.01nnt.com",
    // //正式前端地址*
    // static_host:"https://fs.01nnt.com",
    _token01:"",
    //判断是否注册手机号码,存在则未注册
    _user_mark:"",
    //判断是从那个浏览器进入的
    is_navigator:"",
    //用户进入那个浏览器不用登陆
    no_login:{
        shouye:"/",//首页
        index:"/index",//选择种类页
        selectadress:"/selectadress",//定位
        shopdetails:"/shopdetails",//店铺详情
        dynamic:"/dynamic",//商家圈
        successpm:"/successpm",//付款成功以后页面
        businessindex:"/businessindex",//攻略首页
        businessdetails:"/businessdetails",//攻略详情
    },
    address_info:{province:"广东省",city:"深圳市",area:"龙岗区",address:""},
    latitude:"",//纬度
    longitude:"",//经度
    address_status:"请选择您的位置",//定位状态
    people_info:"",//people_info用来请求分享信息,分享链接需要
    binPeople_id:""//binPeople_id分享出来的People_id
}

// mutations用来改变store状态,  如果访问他的话，就叫访问触发状态
const mutations = {
    //这里面的方法是用 this.$store.commit('jia') 来触发
    setUser(state,newUser){
        state.userInfo.nickname = newUser.nickname;
        state.userInfo.headimgurl = newUser.headimgurl;
    },
    setLocation(state,newLocation){
        state.latitude = newLocation.latitude;
        state.longitude = newLocation.longitude;
    }
}
//暴露到外面，让其他地方的引用
export default new Vuex.Store({
    state,
    mutations
})
