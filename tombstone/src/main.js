// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// By default we import all the components.
// Only reserve the components on demand and remove the rest.
// Style is always required.
import {
  Style,
  // basic
  Button,
  Loading,
  Tip,
  Toolbar,
  TabBar,
  TabPanels,
  // form
  Checkbox,
  CheckboxGroup,
  Radio,
  RadioGroup,
  Input,
  Textarea,
  Select,
  Switch,
  Rate,
  Validator,
  Upload,
  Form,
  // popup
  Popup,
  Toast,
  Picker,
  CascadePicker,
  DatePicker,
  TimePicker,
  SegmentPicker,
  Dialog,
  ActionSheet,
  Drawer,
  ImagePreview,
  // scroll
  Scroll,
  Slide,
  IndexList,
  Swipe,
  Sticky,
  ScrollNav,
  ScrollNavBar,
  RecycleList
} from 'cube-ui'
import App from './App'
import router from './router'
import store from './store/index'
import VueCookies from 'vue-cookies'
import axios from 'axios'
import VueMeta from 'vue-meta'
import qs from 'qs'
import wx from 'weixin-js-sdk';
// import vConsole from '@/util/vconsole.js'


Vue.use(Button)
Vue.use(Loading)
Vue.use(Tip)
Vue.use(Toolbar)
Vue.use(TabBar)
Vue.use(TabPanels)
Vue.use(Checkbox)
Vue.use(CheckboxGroup)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Input)
Vue.use(Textarea)
Vue.use(Select)
Vue.use(Switch)
Vue.use(Rate)
Vue.use(Validator)
Vue.use(Upload)
Vue.use(Form)
Vue.use(Popup)
Vue.use(Toast)
Vue.use(Picker)
Vue.use(CascadePicker)
Vue.use(DatePicker)
Vue.use(TimePicker)
Vue.use(SegmentPicker)
Vue.use(Dialog)
Vue.use(ActionSheet)
Vue.use(Drawer)
Vue.use(ImagePreview)
Vue.use(Scroll)
Vue.use(Slide)
Vue.use(IndexList)
Vue.use(Swipe)
Vue.use(Sticky)
Vue.use(ScrollNav)
Vue.use(ScrollNavBar)
Vue.use(RecycleList)

Vue.use(VueCookies)
Vue.use(VueMeta)

// axios 配置
axios.defaults.timeout = 5000;  //设置超时时间
axios.defaults.baseURL = store.state.host; //这是调用数据接口
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.prototype.$axios = axios;

Vue.prototype.$qs = qs;

Vue.config.productionTip = false


// router.beforeEach((to, from, next) => {
//     store.state._token01 = window.$cookies.get('_token01');
//     store.state._user_mark = window.$cookies.get('_user_mark');
//     // 防止用户授权之后点击返回
//     if(to.path === '/author' && store.state.userInfo.nickname){
//         next('/');
//         return false;
//     }
//     //没登录，没注册手机号，没用户信息都需要跳转中间件页面
//     if(!store.state.userInfo.nickname && to.path != '/author'){
//         store.state.toUrl = to.fullPath;
//         store.state.isRegister = to.path;
//         store.state.binPeople_id = to.query.people_id;
//         next('/author');
//         // return false;
//     }
//     if (store.state.userInfo.nickname) {
//         next();
//         return false
//     }
//     next();
// });

router.beforeEach((to, from, next) => {
    var _token01 = window.$cookies.get('_token01');
    store.state._token01 = _token01;
    var _user_mark = window.$cookies.get('_user_mark');
    store.state._user_mark =_user_mark;

    var is_navigator = "wechat_user"
    store.state.is_navigator = is_navigator;
    //中间获取信息页面和绑定信息页面就不记录进来的页面
    if(to.path != "/author" && to.path !="/binzerone"){
        store.state.toUrl = to.fullPath;
    }
    // 防止用户授权登录之后点击返回binzerone页面
    if(to.path === '/binzerone' && store.state.userInfo.nickname && _token01){
        next('/');
        return false;
    }
    //存推广粉丝ID，注册时绑定
    if(to.query.people_id){
        window.$cookies.set("binPeople_id",to.query.people_id)
    }
    //_token01没账号信息_user_mark没有账号信息并且没有授权
    if(_token01=== null && _user_mark === null){
        // store.state.binPeople_id = to.query.people_id;
        var navigator_url = "wechat_fang";
        let host  = store.state.host;
        let static_host  = store.state.static_host;
        let toUrl  = "/shop"+store.state.toUrl;
        let Base64 = require('js-base64').Base64;
        static_host = Base64.encode(static_host + toUrl);
        window.location.href = host+"/fang/auth/"+navigator_url+"?redirect_uri="+static_host;
    }else{
        //授权以后没有注册
        if(_user_mark){
            //这些页面不需要注册
            if(to.path == store.state.no_login.index ||
                to.path == store.state.no_login.shouye ||
                to.path == store.state.no_login.selectadress ||
                to.path == store.state.no_login.shopdetails ||
                to.path == store.state.no_login.successpm ||
                to.path == store.state.no_login.businessindex ||
                to.path == store.state.no_login.businessdetails ||
                to.path == store.state.no_login.dynamic)
                {
                    next();
                    // return false
                }else{
                    //判断是否进入的是这个页面，否者会循环卡死
                    if(to.path == '/binzerone'){
                        next();
                        // return false
                    }else{
                        next('/binzerone');
                    }
                }
        //存在_token01(存在账号信息)但是没有查询用户信息
        }else if (_token01 && !store.state.userInfo.nickname) {
            // if(!store.state.binPeople_id && to.query.people_id){
            //     store.state.binPeople_id = to.query.people_id;
            // }
            if(to.path == '/author'){
                next();
                // return false
            }else{
                next('/author');
            }
        //存在_token01并且存在用户信息
        }else{
            next();
        }
    }
});

router.afterEach((to, from) => {
  let _url = window.location.origin+"/shop"+ to.fullPath;

  // 非ios设备，切换路由时候进行重新签名
  if (window.__wxjs_is_wkwebview !== true) {
    axios.post('/fang/personal/wechat_config',qs.stringify({uri:_url})).then(function (res) {
      // 注入配置
      wx.config({
          debug: false,
          appId: res.data.data.item.appId,
          timestamp: res.data.data.item.timestamp,
          nonceStr: res.data.data.item.nonceStr,
          signature: res.data.data.item.signature,
          jsApiList: ["updateAppMessageShareData",'updateTimelineShareData','getLocation',"onMenuShareAppMessage","onMenuShareTimeline","onMenuShareWeibo"]
      })
    })
  }
  // ios 设备进入页面则进行js-sdk签名
  if (window.__wxjs_is_wkwebview === true) {
    let _url = window.location.href.split('#')[0];
    axios.post('/fang/personal/wechat_config',qs.stringify({uri:_url})).then(function (res) {
      wx.config({
          debug: false,
          appId: res.data.data.item.appId,
          timestamp: res.data.data.item.timestamp,
          nonceStr: res.data.data.item.nonceStr,
          signature: res.data.data.item.signature,
        jsApiList: ['updateAppMessageShareData','updateTimelineShareData','getLocation',"onMenuShareAppMessage","onMenuShareTimeline","onMenuShareWeibo"]
      })
    })
  }
  let totitle = "";
  let todesc = "";
  if(typeof(to.meta.fenxiang) === "undefined"){
      totitle = "就差你了，快来领取优惠券！";
      todesc = "听说手快的人都点了，快跟我一起来抢券吧~";
  }else{
      totitle = to.meta.fenxiang.title;
      todesc = to.meta.fenxiang.desc;
  }
  //绑粉参数
  if(store.state.people_info && store.state.people_info.id){
      if(_url.indexOf('?')>0){
          _url = _url+"&people_id="+store.state.people_info.id;
      }else{
           _url = _url+"?people_id="+store.state.people_info.id;
      }

  }
  let fenxiang = {
      title: totitle, // 分享标题
      desc: todesc, // 分享描述
      link: _url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致http://static.01nnt.com/shop/lingyilogo.jpg
      imgUrl: 'https://fs.01nnt.com/shop/lingyilogo.jpg', // 分享图标正式http://fs.01nnt.com/shop/lingyilogo.jpg
      success: function () {
        // 设置成功
      }
  }
  // 微信分享配置
  wx.ready(function () {
      wx.updateAppMessageShareData(fenxiang);
      wx.updateTimelineShareData(fenxiang);
      wx.onMenuShareAppMessage(fenxiang);
      wx.onMenuShareTimeline(fenxiang);
      wx.onMenuShareWeibo(fenxiang);
    })
})


//定义全局跳转授权的方法
Vue.prototype.noLogin = function(){
    this.$cookies.remove('_token01','/','.01nnt.com');
    store.state._token01 = "";
    let host = store.state.host;
    let static_host = location.href.split('#')[0];
    let Base64 = require('js-base64').Base64;
    static_host = Base64.encode(static_host);

    if (/MicroMessenger/.test(window.navigator.userAgent)) {
        this.$createDialog({
           type: 'confirm',
           icon: 'cubeic-alert',
           title: '提示信息',
           content: '授权时间已过期，重确认重新授权',
           confirmBtn: {
             text: '确定',
             active: true,
             disabled: false,
             href: 'javascript:;'
           },
           cancelBtn: {
             text: '取消',
             active: false,
             disabled: false,
             href: 'javascript:;'
           },
           onConfirm: () => {
             window.location.href = host+"/fang/auth/wechat_fang?redirect_uri="+static_host;
           },
           onCancel: () => {
               window.location.href = host+"/fang/auth/wechat_fang?redirect_uri="+static_host;
           }
         }).show()
    }
    else if (/AlipayClient/.test(window.navigator.userAgent)) {
        let txt = "请使用微信浏览器进入!";
        this.toast = this.$createToast({
          txt: txt,
          type: 'txt'
        })
        this.toast.show()
        // this.$createDialog({
        //    type: 'confirm',
        //    icon: 'cubeic-alert',
        //    title: '提示信息',
        //    content: '授权时间以过期，重确认重新授权',
        //    confirmBtn: {
        //      text: '确定',
        //      active: true,
        //      disabled: false,
        //      href: 'javascript:;'
        //    },
        //    cancelBtn: {
        //      text: '取消',
        //      active: false,
        //      disabled: false,
        //      href: 'javascript:;'
        //    },
        //    onConfirm: () => {
        //      window.location.href = host+"/fang/auth/ali_fang?redirect_uri="+static_host;
        //    },
        //    onCancel: () => {
        //        window.location.href = host+"/fang/auth/wechat_fang?redirect_uri="+static_host;
        //    }
        //  }).show()
    }else{//否者没有存在是在其他浏览器打开的
        let txt = "请使用微信浏览器进入!";
        this.toast = this.$createToast({
          txt: txt,
          type: 'txt'
        })
        this.toast.show()
    }
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
